//
//  Assignment_1_Milestone_2UITests.swift
//  Assignment 1 Milestone 2UITests
//
//  Created by Alex Alduk on 28/06/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import XCTest

class Assignment_1_Milestone_2UITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = true
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIgnoreThisTest() {
        // Get a reference to your app
        let app = XCUIApplication()
        
        
        //let imageCount = app.images.count
        //let butCount = app.buttons.count
        
        // Test that the initial label text is what you expect
        let pickLabel = app.staticTexts.elementMatchingType(.Any, identifier: "pickButtLabel").label
        XCTAssertEqual(pickLabel, "Generate Playlist")
        
        let historyString = app.staticTexts.elementMatchingType(.Any, identifier: "historyLabel").label
        XCTAssertEqual(historyString, "Playlist History")
        
        let appTitle = app.staticTexts.elementMatchingType(.Any, identifier: "appTitle").label
        XCTAssertEqual(appTitle, "Moodify")
        
        XCTAssert(app.staticTexts["Moodify"].exists)
        
      
        
        
        // Test that there is only 2 button on the view
        XCTAssertEqual(app.buttons.count, 4)
        // Test that there is only 1 image dispayed on the view for Logo
        XCTAssertEqual(app.images.count, 4)
    }
    
    func testPhotoPick() {
        
        let app = XCUIApplication()
        
        // Test elements in HomeViewController
        XCTAssert(app.buttons["logo"].exists)
        XCTAssert(app.staticTexts["Moodify"].exists)
        XCTAssert(app.staticTexts["or"].exists)
        XCTAssert(app.staticTexts["line"].exists)
        
        app.buttons["pickPhotoButton"].tap()
        
        // Photo Picker
        app.tables.buttons["Camera Roll"].tap()
        app.collectionViews["PhotosGridView"].cells.elementBoundByIndex(2).tap()
        
        
       
        
        // Test elements in SelfieSelectViewController
        XCTAssert(app.buttons["logo"].exists)
        XCTAssert(app.staticTexts["Moodify"].exists)
        XCTAssert(app.staticTexts["or"].exists)
        XCTAssert(app.staticTexts["line"].exists)
        
        //var photo = app.buttons["pickPhotoButton"]

        //XCTAssert(app.buttons["analyseMoodButton"].exists)
        app.buttons["analyseMoodButton"].tap()
        
        // Test elements in SelfieScanViewController
        XCTAssert(app.images["logo"].exists)
        XCTAssert(app.staticTexts["Moodify"].exists)
        XCTAssert(app.staticTexts["Detecting Mood  "].exists)
        XCTAssert(app.staticTexts["line"].exists)
        XCTAssert(app.staticTexts["Cancel Scan"].exists)
        XCTAssert(app.buttons["moodResult"].exists)
        XCTAssertEqual(app.buttons["moodResult"].hittable,false)
        XCTAssert(app.activityIndicators.element.exists)
        //var before = app.staticTexts["Detecting Mood  "].label
        
        //Wait for mood to be detected
        sleep(5)
        
        // Labels don't detect the change in text
//        XCTAssert(app.staticTexts["Detecting Mood  "].exists)
//        XCTAssert(app.staticTexts["Generate Playlist"].exists)
        
        var after = app.staticTexts["Detecting Mood  "].label
        XCTAssertEqual(app.activityIndicators.element.hittable,false)
        app.buttons["moodResult"].tap()
        
       // Test elements in Spotify Logon
        XCTAssert(app.staticTexts["Username"].exists)
        XCTAssert(app.staticTexts["Password"].exists)
        
        
    }
}
