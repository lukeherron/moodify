//
//  GeneratedPlaylistViewController.swift
//  Assignment 1 Milestone 2
//
//  Created by Luke on 06/07/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class GeneratedPlaylistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var json: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load the 'generate_playlist.json' file so that we can parse it with SwiftyJSON
        if let path = NSBundle.mainBundle().pathForResource("generated_playlist", ofType: "json") {
            if let data = NSData(contentsOfFile: path) {
                json = JSON(data: data)["tracks"] // Ensure we only extract the 'tracks' elements
            }
        }
        
        // Adjust the seperator line colours to match the header colour
        // TODO: this colour will be set dynamically based on the matched mood (and the colour that the mood matches to on the Plutchik Wheel)
        self.tableView.separatorColor = UIColor(red: 255/255, green: 230/255, blue: 97/255, alpha: 1.0)
        
        // Add an offset to the bottom, so that the toolbar doesn't hide the last row
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     * This method is required as part of the UITableViewDataSource extension. It allows us to create a table cell for each
     * row of the TableView (providing a convenient indexPath which we can use to determine which row we are on)
     */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "spotifyTrackCell" // Table view cells are reused and should be dequeued using a cell identifier.
        let cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SpotifyTrackCell
        let row: Int = indexPath.row
        let imgURL = NSURL(string: json[row]["album","images",1,"url"].stringValue)
        
        // The track time is expressed in milliseconds, so format it into something a bit more readable before displaying it
        let milliseconds: Int = json[row]["duration_ms"].intValue
        let seconds: Int = (milliseconds / 1000) % 60;
        let minutes: Int = (milliseconds / 1000) / 60;
        
        // Time to fill all of our cell info with the JSON data
        cell.albumArt.image = UIImage(data: NSData(contentsOfURL: imgURL!)!)
        cell.trackName.text = json[row]["name"].stringValue
        cell.albumName.text = json[row]["album","name"].stringValue
        cell.artistName.text = json[row]["artists",0,"name"].stringValue
        cell.trackLength.text = String(format: "%d:%02d", minutes, seconds)
        cell.explicit.hidden = !json[row]["explicit"].boolValue

        return cell
    }
    
    /**
     * Another method required for the UITableViewDataSource extension. Essentially tells the data source how many rows are to be expected
     */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return json.count
    }
}