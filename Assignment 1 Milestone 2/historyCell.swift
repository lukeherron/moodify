//
//  historyCell.swift
//  Assignment 1 Milestone 2
//
//  Created by Alex Alduk on 16/07/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class historyCell: UITableViewCell {

    @IBOutlet var historyTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
