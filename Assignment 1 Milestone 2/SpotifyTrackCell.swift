    //
//  SpotifyTrackCell.swift
//  Assignment 1 Milestone 2
//
//  Created by Luke on 06/07/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class SpotifyTrackCell: UITableViewCell {

    @IBOutlet weak var albumArt: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var trackLength: UILabel!
    @IBOutlet weak var explicit: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        explicit.layer.borderWidth = 0.1
        explicit.layer.cornerRadius = 2
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
