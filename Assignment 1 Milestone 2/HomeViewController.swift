//
//  FirstViewController.swift
//  Assignment 1 Milestone 2
//
//  Created by Alex Alduk on 28/06/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var photo:UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Apply a round border to pickPhotoButton - commented out as this behaviour changes with different sizes and different orientations. Have created an image that has the border already instead
        //pickPhotoButton.layer.cornerRadius = pickPhotoButton.frame.height/2
        //pickPhotoButton.layer.borderWidth = 3
        //pickPhotoButton.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    // Pick photo from picker and segue to next view
    @IBAction func pickPhoto(sender: UIButton) {
        let pickPhoto = UIImagePickerController()
        pickPhoto.delegate = self
        pickPhoto.sourceType = .PhotoLibrary
        // Display photo picker
        self.presentViewController(pickPhoto, animated: true, completion: nil)
    }
    
    @IBAction func viewHistory(sender: UIButton) {
        // iOS won't allow us to push a UISplitViewController onto a UIViewController. Generally the UISplitViewController needs to
        // be the root view controller, so we will programatically change our root view controller and provide an animation to mimic
        // a segue transition
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let historyViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("historyRootViewController")
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let window = appDelegate.window {
            UIView.transitionWithView(window, duration: 0.75, options: .TransitionFlipFromLeft, animations: {
                () -> Void in window.rootViewController = historyViewController
            }, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Assign photo to var and close photo picker
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // Assign choosen photo to var photo
        photo = info[UIImagePickerControllerOriginalImage] as? UIImage
        // Close photo picker view
        self.dismissViewControllerAnimated(false, completion: nil)
        // Segue to next view
        performSegueWithIdentifier("pickComplete", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "pickComplete" {
            if let destination = segue.destinationViewController as? SelfieSelectViewController {
                destination.photo = photo
            }
        }
    }
}