//
//  HistoryViewController.swift
//  Assignment 1 Milestone 2
//
//  Created by Luke on 12/07/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class PlaylistHistoryMasterViewController: UITableViewController, UISplitViewControllerDelegate {
    
    var tableData: [String] = ["Playlist 1", "Playlist 2", "Playlist 3", "Playlist 4"] // Any string data just to populate the rows.....
    
    @IBOutlet var historyTableView: UITableView!
    
    @IBAction func pickSelfie(sender: UIBarButtonItem) {
        // iOS won't allow us to push a UISplitViewController onto a UIViewController. Generally the UISplitViewController needs to
        // be the root view controller, so we will programatically change our root view controller and provide an animation to mimic
        // a segue transition
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let selfieViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("selfieRootViewController")
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let window = appDelegate.window {
            UIView.transitionWithView(window, duration: 0.75, options: .TransitionFlipFromRight, animations: {
                () -> Void in window.rootViewController = selfieViewController
            }, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.splitViewController?.delegate = self
        self.splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        
        let nib = UINib(nibName: "historyCell", bundle: nil)
        historyTableView.registerNib(nib, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(historyTableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(historyTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    
    override func tableView(historyTableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell" // Table view cells are reused and should be dequeued using a cell identifier.
        let cell = self.historyTableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! historyCell

            //let cellIdentifier = "spotifyTrackCell" // Table view cells are reused and should be dequeued using a cell identifier.
           // let cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SpotifyTrackCell
        // PortraitHistoryCell. = self.tableData[indexPath.row]
        //PortraitHistoryCell.textLabel?.text = tableData[indexPath.row]
        cell.historyTextLabel.text = self.tableData[indexPath.row]
        
        return cell
    }
    
    override func tableView(historyTableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let selectedPlaylist = self.tableData[indexPath.row]
        //self.delegate?.playlistSelected(selectedPlaylist)
        
        //if let detailViewController = self.delegate as? PlaylistHistoryDetailViewController {
        //    splitViewController?.showDetailViewController(detailViewController as UIViewController, sender: nil)
        //}
        self.performSegueWithIdentifier("showDetail", sender: self)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
     // MARK: - Navigation
    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            // Get the new view controller using segue.destinationViewController.
            // Pass the selected object to the new view controller.
        }
    }
     */
    
    // On smaller screen devices, collapse the detail view. This effectively means the Master will display first on iPhones
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return true
    }
}
