//
//  SecondViewController.swift
//  Assignment 1 Milestone 2
//
//  Created by Alex Alduk on 28/06/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class SelfieSelectViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var lineLabel: UILabel!
    
    @IBAction func changeSelfie(sender: AnyObject) {
        let pickPhoto = UIImagePickerController()
        pickPhoto.delegate = self
        pickPhoto.sourceType = .PhotoLibrary
        // Display photo picker
        self.presentViewController(pickPhoto, animated: true, completion: nil)
    }
    
    // View for photo that was selected by user
    var photo:UIImage? = nil
    var unwinding = false
    
    // update UI
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set button image to photo that was choosen
        button.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
        button.setImage(photo, forState: UIControlState.Normal)
        // Convert rectangle image to circle
        button.layer.cornerRadius = button.frame.height / 2
        // Trim excess image off corners
        button.clipsToBounds = true
        // Set border width and colour
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.blackColor().CGColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "scanImage" {
            if let destination = segue.destinationViewController as? SelfieScanViewController {
                destination.photo = photo
            }
        }
    }
    
    // Assign photo to var and close photo picker
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // Assign choosen photo to var photo
        photo = info[UIImagePickerControllerOriginalImage] as? UIImage
        // Close photo picker view
        self.dismissViewControllerAnimated(false, completion: nil)
        // Refresh view
        viewDidLoad()
    }
    
    @IBAction func unwindToVC(segue:UIStoryboardSegue) {
        if(segue.sourceViewController.isKindOfClass(SelfieScanViewController)) {
            self.unwinding = true
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if (self.unwinding) {
            let pickPhoto = UIImagePickerController()
            pickPhoto.delegate = self
            pickPhoto.sourceType = .PhotoLibrary
            // Display photo picker
            self.presentViewController(pickPhoto, animated: true, completion: nil)
            self.unwinding=false
        }
    }
    
}

