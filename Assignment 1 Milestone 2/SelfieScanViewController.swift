//
//  ThirdViewController.swift
//  Assignment 1 Milestone 2
//
//  Created by Phillip Attard on 8/07/2016.
//  Copyright © 2016 Alex Alduk. All rights reserved.
//

import UIKit

class SelfieScanViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    // View for photo that was selected by user
    
    var photo:UIImage? = nil
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var activityMonitor: UIActivityIndicatorView!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var moodResult: UIButton!
    
    // Temporary var for Assignment 1 milestone 2 timers
    var timer = NSTimer()
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set button image to photo that was choosen
        button.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
        button.setImage(photo, forState: UIControlState.Normal)
        // Convert rectangle image to circle
        button.layer.cornerRadius = button.frame.height / 2
        // Trim excess image off corners
        button.clipsToBounds = true
        // Set border width and colour
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.blackColor().CGColor
        
        // hide activity monitor and mood result
        activityMonitor.hidden = true
        moodResult.hidden = true
        // start timer
        activity()
    }
    
    // Temporary timer method for assignment 1 milestone 2
    func activity(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target:self, selector: #selector(updateCounter), userInfo: nil, repeats: true  )
    }
    
    // Temporary counter for assignment 1 milestone 2
    // Make UI changes when counter reaches 1 and 4 secs
    func updateCounter() {
        // increment counter by 1
        counter = counter + 1
        // when counter reaches 1 display activity monitor
        if counter == 1 {
            activityMonitor.hidden = false
        }
        // when counter reaches 4
        if counter == 4 {
            // Hide activity monitor
            activityMonitor.hidden = true
            // reveal mood result
            moodResult.hidden = false
            // update labels
            lineLabel.text = "Mood Detected"
            bottomLabel.text = "Generate Playlist"
            // Set Background colours to yellow
            self.view.backgroundColor = UIColor.yellowColor()
            lineLabel.backgroundColor = UIColor.yellowColor()
            // Deactivate timer
            timer.invalidate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
